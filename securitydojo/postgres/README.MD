# local postgres db for dojo testing
with the following commands you can start the local postgres db
```
# build and startup
docker-compose up --build -d

# inspect the logs
docker logs -f postgres_postgres_1

# kill and erase data
docker rm -f $(docker ps -a -q) && docker volume rm $(docker volume ls -q)
```

Connect using ...
Database: dojo
Username: dojo
Password: secret