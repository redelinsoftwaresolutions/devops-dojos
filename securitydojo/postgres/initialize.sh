#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE USER dojo WITH PASSWORD 'test101';
    CREATE DATABASE dojo;
    GRANT ALL PRIVILEGES ON DATABASE dojo TO dojo;
EOSQL

psql -v ON_ERROR_STOP=1 --username "dojo" <<-EOSQL
	create table users (name varchar(50), email varchar(50), password varchar(50));
	insert into users (name, email, password) values ('Maria Mustermann', 'maria@mustermann.de', 'mySuperSecret');
	insert into users (name, email, password) values ('Max Mustermann', 'max@mustermann.de', 'mySuperSecret2');

	create table secretsoftheworld (owner varchar(50), secret varchar(50));
	insert into secretsoftheworld (owner, secret) values ('Amazon', '123htggsH#!4');
	insert into secretsoftheworld (owner, secret) values ('Otto', '123htggsH#!5');
	insert into secretsoftheworld (owner, secret) values ('Zalando', '123htggsH#!6');
EOSQL