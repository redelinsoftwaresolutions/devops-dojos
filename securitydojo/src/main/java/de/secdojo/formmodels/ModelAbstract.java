package de.secdojo.formmodels;

public abstract class ModelAbstract {
    String errorMessageI18n;
    String successMessageI18n;

    public String getErrorMessageI18n() {
        return errorMessageI18n;
    }

    public void setErrorMessageI18n(String errorMessageI18n) {
        this.errorMessageI18n = errorMessageI18n;
        this.successMessageI18n = null;
    }

    public String getSuccessMessageI18n() {
        return successMessageI18n;
    }

    public void setSuccessMessageI18n(String successMessageI18n) {
        this.successMessageI18n = successMessageI18n;
        this.errorMessageI18n = null;
    }

    public void clearMessages() {
        this.successMessageI18n = "";
        this.errorMessageI18n = "";
    }
}
