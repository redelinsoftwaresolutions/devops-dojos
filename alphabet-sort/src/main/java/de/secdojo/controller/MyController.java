package de.secdojo.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import de.secdojo.formmodels.Login;

@Controller
public class MyController {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		setModelAttributes(model, new Login(), locale);
		return "home";
	}

	private String getCurrentTime(Locale locale) {
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		return dateFormat.format(date);
	}

	private void setModelAttributes(Model model, Login loginModel, Locale locale) {
		model.addAttribute("loginModel", loginModel);
		model.addAttribute("serverTime", getCurrentTime(locale));
	}
}
