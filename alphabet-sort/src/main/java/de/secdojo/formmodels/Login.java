package de.secdojo.formmodels;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class Login extends ModelAbstract {
    private String email;
    private String password;
}
