package de.secdojo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecuritydojoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecuritydojoApplication.class, args);
	}
}
